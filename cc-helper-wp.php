<?
/**
 * @link    https://gitlab.com/himalayan-institute/cc-helper-wp
 * @package CC Helper WP
 * @author  Rafe Colton <rcolton@himalayaninstitute.org>
 *
 * @wordpress-plugin
 * Plugin Name:       CC Helper WP
 * Plugin URI:        https://gitlab.com/himalayan-institute/cc-helper-wp
 * Description:       Enqueues JS for formatting credit card number input fields
 * Version:           0.1.0
 * Author:            Rafe Colton | Himalayan Institute
 * Author URI:        http://rafecolton.com
 * License:           MIT
 * GitLab Plugin URI: https://gitlab.com/himalayan-institute/cc-helper-wp
 */

defined( 'ABSPATH' ) or die( 'NO, HUMAN' );

// constants
define( 'CCH_INCLUDES', plugin_dir_path( __FILE__ ) . '/includes' );
define( 'CCH_INCLUDES_URI', plugin_dir_url( __FILE__ ) . 'includes' );
define( 'CCH_ASSETS_URI', plugin_dir_url( __FILE__ ) . 'assets' );

function cch_register_enqueue_assets() {
  wp_register_style( 'cc-helper-wp-css', CCH_ASSETS_URI . '/css/cc-helper.css' );
  wp_enqueue_style( 'cc-helper-wp-css' );

  wp_register_script( 'cc-helper-wp-js', CCH_ASSETS_URI . '/js/cc-helper.js', array( 'jquery' ) );
  wp_enqueue_script( 'cc-helper-wp-js' );
}

add_action( 'wp_enqueue_scripts', 'cch_register_enqueue_assets' );
