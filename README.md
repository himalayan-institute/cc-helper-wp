💥💥**DEPRECATED:** Don't use this!! Use [`cleave.js`](https://github.com/nosir/cleave.js)—it is much better.💥💥

CC Helper WP
============

WordPress plugin for enqueueing [@cyhtan](https://github.com/cyhtan)'s
[credit card helper script](https://github.com/cyhtan/cc-helper/tree/master).

Usage:
------

1. Install plugin
1. Apply JS code in page per [cc-helper README](https://github.com/cyhtan/cc-helper/tree/master)
